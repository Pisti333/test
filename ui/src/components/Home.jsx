import React from 'react';

const Home = () => {
    const onSubmit = (e) => {
        e.preventDefault();
        const name = e.target.name.value;
        e.target.name.value = '';
        alert(`Hello, ${name}!`);
    };
    return (
        <div style={{ textAlign: 'center', marginTop: '50px' }}>
            <h1 style={{ fontSize: '24px' }}>Welcome to the Home component!</h1>
            <form onSubmit={onSubmit}>
                <input type="text" name="name" id="name" placeholder="Enter your name" style={{ padding: '10px', borderRadius: '5px', marginRight: '10px' }} />
                <button style={{ padding: '10px 20px', borderRadius: '5px', backgroundColor: '#007bff', color: '#fff', border: 'none' }}>Submit</button>
            </form>
        </div>
    );
};

export default Home;    